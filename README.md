# Gorgon 1.0
*by the Lobdegg*

Parser-based adventure game engine and its related tools. Written by the Lobdegg during the [DOS Games Spring Jam 2021](https://itch.io/jam/dos-games-spring-jam-2021), Gorgon is an AGI-inspired graphical adventure game engine driven by parser input. It is the engine behind [Hadrosaurus Software](https://www.hadrosaurus.net/)'s first release, *[The Aching](https://store.steampowered.com/app/2295210/The_Aching/)*.

Gorgon 1.0 is compiled with Borland Turbo C++, which can currently be downloaded from [archive.org](https://archive.org/details/tcc_20210425). In Borland, open the `.PRJ` file to compile.

All programs and libraries contained within the `tools` folder are compiled with Open Watcom.

## Included tools

| Tool       | Description |
| ------     | ------ |
| **`imagelib`** | Contains code for loading 256 color indexed `.BMP` and `.PCX` images that can be used by `imgpackr` and `scrpackr`. |
| **`imgpackr`** | Parses a spritesheet into a file containing a series of images. |
| **`scriptor`** | Compiles Lobdegg Script files into the `.CNS` byte code format used by both engines. |
| **`scrpackr`** | Builds a package file containing full screen images with the option of raw or run-length-encoded data. |
| **`strpackr`** | Divides a raw text file into an array of strings |
| **`tandysnd`** | Contains both the TandyPacker and TandyPlayer tools for assembling Tandy 3 Voice Sound compatible data. |
