#include <alloc.h>
#include <dos.h>
#include <stdio.h>

unsigned short Volume = 100;
unsigned short Speed = 75;

#define SOUNDPACK_MAGICNUMBER		0x20646E53

typedef struct {
	unsigned long int MagicNumber;
	unsigned short Reserved;
	unsigned char BitWidth;
	unsigned char Count;
} SoundPackHeader_t;

typedef struct {
	unsigned long int Offset;
	unsigned long int Length;
} SoundEntry_t;

struct {
	FILE *fp;
	SoundPackHeader_t hdr;
	SoundEntry_t *table;
	unsigned char *data;
} pack = {0};

int LoadPack(const char *filename) {
	pack.fp = fopen(filename,"rb");
	if (pack.fp==NULL) {
		printf("Failed to open file '%s'!\n",filename);
		return 1;
	}
	fread(&pack.hdr,1,sizeof(SoundPackHeader_t),pack.fp);
	if (pack.hdr.MagicNumber!=SOUNDPACK_MAGICNUMBER) {
		printf("Invalid sound pack!\n");
		return 2;
	}
	pack.table = (SoundEntry_t*)malloc(pack.hdr.Count * 8);
	if (pack.table==NULL) {
		printf("Failed to allocate memory for table!\n");
		return 3;
	}
	fread(pack.table,1,pack.hdr.Count*8,pack.fp);
	return 0;
}

int LoadSound(int idx) {
	//printf("%lX %lX\n",pack.table[idx].Offset,pack.table[idx].Length);
	pack.data = (unsigned char*)malloc(pack.table[idx].Length);
	if (pack.data==NULL) {
		printf("Failed to allocate sound!\n");
		return 1;
	}
	fseek(pack.fp,pack.table[idx].Offset,SEEK_SET);
	fread(pack.data,1,pack.table[idx].Length,pack.fp);
	return 0;
}

short Adjust(short s) {
	s&=0x0FF;
	s-=128;
	s*=Volume;
	s/=100;
	if (s>127) s=127;
	if (s<(-128)) s=-128;
	s+=128;
	return (s>>2)+1;
}

short Sample(short s) {
	unsigned short basetimer;
	unsigned short curtimer;
	s = Adjust(s);
	outp(0x42,(char)s);
	disable();
	outp(0x43,0);
	basetimer = inp(0x40)|(inp(0x40)<<8);
	
	do {
	disable();
	outp(0x43,0);
	curtimer = inp(0x40)|(inp(0x40)<<8);
	enable();
	} while ((basetimer - curtimer) < Speed);
	
	return s;
}

void PlaySound(short idx) {
	unsigned char *ptr, *end;
	
	if (idx<0 || idx>=pack.hdr.Count) {
		printf("Index out of range!\n");
		return;
	}
	if (LoadSound(idx)!=0) {
		return;
	}
	
	ptr = pack.data;
	end = ptr + pack.table[idx].Length;
	
	outp(0x43,0x90);//Select timer 2 in one-shot single byte mode
	outp(0x61,inp(0x61)|3);//Turn on speaker bits
	
	do {
		if (pack.hdr.BitWidth==4) {
			Sample(((*ptr)&0x0F)<<2);
			Sample(((*ptr)&0xF0)>>2);
		} else if (pack.hdr.BitWidth==8) {
			Sample(*ptr);
		}
		ptr++;
	} while(ptr<end);
	
	outp(0x61,inp(0x61)&0xFC);
}

int main(int argc, char **argv) {
	int c, idx=0;
	for (c=1; c<argc; c++) {
		if (strcmp(argv[c],"-s")==0) {//Speed
			c++;
			if (c>=argc)
				break;
			Speed = atoi(argv[c]);
			printf("Speed set to %d\n",Speed);
		} else if (strcmp(argv[c],"-v")==0) {//Volume
			c++;
			if (c>=argc)
				break;
			Volume = atoi(argv[c]);
			printf("Volume set to %d\n",Volume);
		} else if (strcmp(argv[c],"-i")==0) {//Sound Index
			c++;
			if (c>=argc)
				break;
			idx = atoi(argv[c]);
			printf("Index set to %d\n",idx);
		} else {
			LoadPack(argv[c]);
		}
	}
	if (pack.fp==NULL) {
		return 2;
	}
	
	PlaySound(idx);
	
	fclose(pack.fp);
	free(pack.data);
	free(pack.table);
	return 0;
}
