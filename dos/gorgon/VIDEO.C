#include <alloc.h>
#include <dos.h>
#include <stdarg.h>
#include <stdio.h>

#include "gorgon.h"

#define PALETTE_MAGICNUMBER	0x6C70

struct {
	unsigned char far *screen;
	unsigned char *scrn_buffer;
	unsigned char *back_buffer;
	unsigned char *phys_buffer;
	unsigned char *mask_buffer;
	unsigned char *target_buffer;
	unsigned char mode;
	short start, stop;
} video = {0};

typedef struct {
	unsigned short MagicNumber;
	unsigned short Count;
} palette_header_t;

typedef struct {
	unsigned char r[16];
	unsigned char g[16];
	unsigned char b[16];
	unsigned short hilo;
} palette_entry_t;

struct {
	palette_header_t *hdr;
	palette_entry_t *data;
} palette = {0};

/*****************************************
	Video Functions
*****************************************/
unsigned char OSmode;

unsigned char getvideomode() {
	union REGS regs;
	regs.h.ah=0x0F;
	int86(0x10,&regs,&regs);
	return regs.h.al;
}

void setvideomode(unsigned char mode) {
	union REGS regs;
	regs.h.ah=0;
	regs.h.al=mode;
	int86(0x10,&regs,&regs);
}

bool v_init(unsigned char desiredmode) {
	unsigned int fsize;
	palette_header_t *buffer;
	
	video.stop = 50;
	OSmode = getvideomode();
	setvideomode(desiredmode);
	if (getvideomode()!=desiredmode) {
		err_SetError(ERR_VIDERROR,"Could not establish correct video mode!");
		return FALSE;
	}
	video.mode=desiredmode;
	//Set video memory address
	video.screen = MK_FP(0xB800,0);
	if (desiredmode==MODE_EGA)
		video.screen = MK_FP(0xA000,0);
	
	//allocate memory for screen artwork
	video.scrn_buffer = (unsigned char*)malloc(/*desiredmode==MODE_CGA?16000:*/32000);
	if (video.scrn_buffer==NULL) {
		err_SetError(ERR_MEMORY,"Insufficient memory! A");
		return FALSE;
	}
	memset(video.scrn_buffer,0,/*desiredmode==MODE_CGA?16000:*/32000);
	video.target_buffer = video.scrn_buffer;
	
	//allocate memory for screen physics map
	video.phys_buffer = (unsigned char*)malloc(16000);
	if (video.phys_buffer==NULL) {
		err_SetError(ERR_MEMORY,"Insufficient memory! C");
		return FALSE;
	}
	memset(video.phys_buffer,0,16000);
	
	//allocate memory for screen mask map
	video.mask_buffer = (unsigned char*)malloc(16000);
	if (video.mask_buffer==NULL) {
		err_SetError(ERR_MEMORY,"Insufficient memory! D");
		return FALSE;
	}
	memset(video.mask_buffer,0,16000);

	//allocate memory for back buffer
	video.back_buffer = (unsigned char*)malloc(/*desiredmode==MODE_CGA?16000:*/32000);
	if (video.back_buffer==NULL) {
		err_SetError(ERR_MEMORY,"Insufficient memory! B");
		return FALSE;
	}
	memset(video.back_buffer,0,/*desiredmode==MODE_CGA?16000:*/32000);
	
	spr_init(desiredmode);
	
	buffer = (palette_header_t*)BlockLoad("PALETTES.DAT",&fsize);
	if (buffer!=NULL) {
		if (buffer->MagicNumber!=PALETTE_MAGICNUMBER)
			err_SetError(ERR_INVALIDFILE,"Invalid palettes file!");
		else {
			palette.hdr = buffer;
			palette.data = (palette_entry_t*)(buffer+1);
		}
	}
	return TRUE;
}

void v_quit() {
	//free(video.prirty_buffer);
	free(palette.hdr);
	free(video.phys_buffer);
	free(video.back_buffer);
	free(video.scrn_buffer);
	setvideomode(OSmode);
}

void v_settarget(short channel) {
	if (channel==1)
		video.target_buffer = video.phys_buffer;
	else if (channel==2)
		video.target_buffer = video.mask_buffer;
	else if (channel==3)
		video.target_buffer = video.back_buffer;
	else
		video.target_buffer = video.scrn_buffer;
}

/*****************************************
	Screen Importer Functions
*****************************************/
#define SCREEN_TGA			0x6005
#define SCREEN_EGA			0x6006
#define SCREEN_CGA			0x6007
#define SCREEN_RLE			0x10
typedef struct {
	unsigned short MagicNumber;
	short ScreenCount;
} ScreenHeader_t;

void v_clear() {
	unsigned short entrysize=16000;//8000;
	memset(video.scrn_buffer,0,entrysize<<1);
}

void v_setstartstop(char start, char stop) {
	if (start==stop) {
		video.start = 0;
		video.stop = 50;
	} else {
		video.start = start;
		if (video.start<0) video.start = 0;
		video.stop = stop;
		if (video.stop>50) video.stop = 50;
	}
}

void v_loadscreen(short index) {
	unsigned char filename[]="res1.tga";
	unsigned char buffer[80];
	unsigned short *target;
	unsigned char  *target_rle;
	unsigned short entrysize=16000;//8000;
	unsigned short magicnumber=SCREEN_TGA;//SCREEN_CGA;
	ScreenHeader_t hdr;
	FILE *f;
	int i, j, k, l, m;
	struct {
		unsigned short id;
		unsigned short length;
	} rle_hdr;
	struct {
		unsigned char pixels;
		unsigned char pattern;
	} rle_blitter;
	strcpy(buffer,filename);
	/*if (video.mode==MODE_TGA||video.mode==MODE_EGA) {
		strcpy(buffer+5,"tga");
		entrysize=16000;
		magicnumber=SCREEN_TGA;
	}*/
	f=fopen(buffer,"rb");
	if (f==NULL) {
		err_SetError(ERR_RES1,"Could not load from file '%s'!",buffer);
		ExitGame();
		return;
	}
	fread(&hdr,1,sizeof(ScreenHeader_t),f);
	memset(video.scrn_buffer,0,entrysize<<1);
	if (hdr.ScreenCount>index) {
		if (hdr.MagicNumber==magicnumber) {
			target=(unsigned short*)video.scrn_buffer;
			for (i=0; i<index; i++)
				fseek(f,entrysize,SEEK_CUR);
			for (i=0; i<200; i++) {
				if (video.mode==MODE_CGA) {
					/*fread(buffer,1,40,f);
					for (j=0; j<40; j++) {
						*target = ((((buffer[j]>>6)&3)*5)<<4)|(((buffer[j]>>4)&3)*5)&0x0FF;
						*target |= ((unsigned short)((((buffer[j]>>2)&3)*5)<<4)|(((buffer[j]>>0)&3)*5)) << 8;
						target++;
					}*/
				} else {
					fread(buffer,1,80,f);
					for (j=0; j<80; j++) {
						*target = ((unsigned short)((buffer[j]&0x0F)|((buffer[j]&0x0F)<<4)))&0x0FF;
						*target |= ((unsigned short)((buffer[j]&0xF0)|(((buffer[j]&0xF0)>>4)&0x0F))) << 8;
						target++;
					}
				}
			}
		} else if (hdr.MagicNumber==(magicnumber|SCREEN_RLE)) {
			target_rle=video.scrn_buffer;
			for (i=0; i<index; i++) {
				fread(&rle_hdr,1,sizeof(rle_hdr),f);
				if (rle_hdr.id!=0x3412) {
					err_SetError(ERR_RES1,"Invalid RLE for screen[%d] in '%s'!",i,buffer);
					fclose(f);
					ExitGame();
					return;
				}
				fseek(f,rle_hdr.length,SEEK_CUR);
			}
			fread(&rle_hdr,1,sizeof(rle_hdr),f);
			for (i=0; i<rle_hdr.length; i+=2) {
				fread(&rle_blitter,1,sizeof(rle_blitter),f);
				if (video.mode==MODE_CGA) {
				} else {
					for (j=0; j<rle_blitter.pixels; j++) {
						if (j&1)
							*target_rle=(rle_blitter.pattern&0xF0)|((rle_blitter.pattern>>4)&0x0F);
						else
							*target_rle=(rle_blitter.pattern&0x0F)|(rle_blitter.pattern<<4);
						target_rle++;
					}
				}
			}
		}
	}
	fclose(f);
	//v_drawscreen();
}

void v_loadmap(short index) {
	int i, j, k;
	ScreenHeader_t hdr;
	struct {
		unsigned short id;
		unsigned short length;
	} rle_hdr;
	struct {
		unsigned char pixels;
		unsigned char pattern;
	} rle_blitter;
	unsigned char temp;
	FILE *f=fopen("res1.dat","rb");
	if (f==NULL)
		return;
	fread(&hdr,1,sizeof(ScreenHeader_t),f);
	memset(video.phys_buffer,0,16000);
	if (hdr.ScreenCount>index) {
		if (hdr.MagicNumber==SCREEN_TGA) {
			for (k=0; k<index; k++)
				fseek(f,16000,SEEK_CUR);
			fread(video.phys_buffer,1,16000,f);
		} else if (hdr.MagicNumber==(SCREEN_TGA|SCREEN_RLE)) {
			for (i=0; i<index; i++) {
				fread(&rle_hdr,1,sizeof(rle_hdr),f);
				if (rle_hdr.id!=0x3412) {
					err_SetError(ERR_RES1,"Invalid RLE for screen[%d] in 'res1.dat'!",i);
					fclose(f);
					ExitGame();
					return;
				}
				fseek(f,rle_hdr.length,SEEK_CUR);
			}
			fread(&rle_hdr,1,sizeof(rle_hdr),f);
			k = 0;
			for (i=0; i<rle_hdr.length; i+=2) {
				fread(&rle_blitter,1,sizeof(rle_blitter),f);
				for (j=0; j<rle_blitter.pixels; j++) {
					if (j&1)
						temp=rle_blitter.pattern>>4;
					else
						temp=rle_blitter.pattern;
					temp&=0x0F;
					if (k&1)
						temp<<=4;
					video.phys_buffer[k>>1]|=temp;
					k++;
				}
			}
		}
	}
	fclose(f);
}

void v_loadmask(short index) {
	int i, j, k;
	ScreenHeader_t hdr;
	struct {
		unsigned short id;
		unsigned short length;
	} rle_hdr;
	struct {
		unsigned char pixels;
		unsigned char pattern;
	} rle_blitter;
	unsigned char temp;
	FILE *f=fopen("res1.dat","rb");
	if (f==NULL)
		return;
	fread(&hdr,1,sizeof(ScreenHeader_t),f);
	memset(video.mask_buffer,0,16000);
	if (hdr.ScreenCount>index) {
		if (hdr.MagicNumber==SCREEN_TGA) {
			for (k=0; k<index; k++)
				fseek(f,16000,SEEK_CUR);
			fread(video.mask_buffer,1,16000,f);
		} else if (hdr.MagicNumber==(SCREEN_TGA|SCREEN_RLE)) {
			for (i=0; i<index; i++) {
				fread(&rle_hdr,1,sizeof(rle_hdr),f);
				if (rle_hdr.id!=0x3412) {
					err_SetError(ERR_RES1,"Invalid RLE for screen[%d] in 'res1.dat'!",i);
					fclose(f);
					ExitGame();
					return;
				}
				fseek(f,rle_hdr.length,SEEK_CUR);
			}
			fread(&rle_hdr,1,sizeof(rle_hdr),f);
			k = 0;
			for (i=0; i<rle_hdr.length; i+=2) {
				fread(&rle_blitter,1,sizeof(rle_blitter),f);
				for (j=0; j<rle_blitter.pixels; j++) {
					if (j&1)
						temp=rle_blitter.pattern>>4;
					else
						temp=rle_blitter.pattern;
					temp&=0x0F;
					if (k&1)
						temp<<=4;
					video.mask_buffer[k>>1]|=temp;
					k++;
				}
			}
		}
	}
	fclose(f);
}

unsigned char v_getmapat(short x, short y) {
	short off, shft[2]={0,4};
	if (x<0||x>=160||y<0||y>=200)
		return 0;
	off = (y<<4)+(y<<6)+(x>>1);
	return (video.phys_buffer[off]>>shft[x&1])&0x0F;
}

unsigned char v_getmaskat(short x, short y) {
	short off, shft[2]={0,4};
	if (x<0||x>=160||y<0||y>=200)
		return 0;
	off = (y<<4)+(y<<6)+(x>>1);
	return (video.mask_buffer[off]>>shft[x&1])&0x0F;
}

void v_drawscreen() {
	memcpy(video.back_buffer,video.scrn_buffer,video.mode==MODE_CGA?16000:32000);
}

//ega channels:
//0x01:green
//0x02:red
//0x04:dark red???
//0x08:intensity?
unsigned char ega_filter[4] = {1,2,4,8};
void v_drawbackbuffer() {
	unsigned short offsets[4] = {0,8192,16384,24576};
	unsigned char ega, cga;
	int i, j, k, l, m, n, o;
	/*if (video.mode==MODE_CGA) {
		k=0;
		l=0;
		for (i=0; i<200; i++) {
			for (j=0;j<80;j++) {
				video.screen[offsets[(i&1)]+j+k]=0x1B;//video.back_buffer[j+l];
			}
			if (i&1)
				k+=80;
			l+=80;
		}
	} else*/ if (video.mode==MODE_TGA) {
		m=0;
		for (i=0; i<4; i++) {
			l=video.start*160;
			if (video.target_buffer==video.scrn_buffer||video.target_buffer==video.back_buffer) {
				for (j=video.start; j<video.stop; j++) {
					for (k=0; k<160; k++) {
						video.screen[offsets[i]+k+l]=video.back_buffer[k+(l<<2)+m];
					}
					l+=160;
				}
				m+=160;
			} else {
				n = 0x0F;
				for (j=video.start; j<video.stop; j++) {
					for (k=0; k<160; k++) {
						cga = video.target_buffer[(k>>1)+(l<<1)+m]&n;
						cga|=cga<<4;
						cga|=cga>>4;
						video.screen[offsets[i]+k+l]=cga;
						n^=0xFF;
					}
					l+=160;
				}
				m+=80;
			}
			while (FrameReady()==0) {}
		}
	} else {
		for (i=0; i<4; i++) {
			l = (video.start*160) + (i*40);
			if (video.target_buffer==video.scrn_buffer) {
				m = (video.start*640) + (i*160);
				for (j=video.start; j<video.stop; j++) {
					outp(0x03C4,0x02);
					for (k=0;k<4;k++) {
						outp(0x03C5,1<<k);
						n = 0;
						for(o=0; o<40; o++) {
							ega = (video.back_buffer[m+n]>>k)&1;
							ega = ((video.back_buffer[m+n+1]>>k)&1)|(ega<<2);
							ega = ((video.back_buffer[m+n+2]>>k)&1)|(ega<<2);
							ega = ((video.back_buffer[m+n+3]>>k)&1)|(ega<<2);
							ega |= ega<<1;
							video.screen[l+o]=ega;
							n += 4;
						}
					}
					l+=160;
					m+=640;
				}
			} else {
				m = (video.start*320) + (i*80);
				for (j=video.start; j<video.stop; j++) {
					outp(0x03C4,0x02);
					for (k=0;k<4;k++) {
						outp(0x03C5,ega_filter[k]);
						for(o=0; o<40; o++) {
							n = o<<1;
							ega = (video.target_buffer[m+n]>>k)&1;
							ega = ((video.target_buffer[m+n]>>(k+4))&1)|(ega<<2);
							ega = ((video.target_buffer[m+n+1]>>k)&1)|(ega<<2);
							ega = ((video.target_buffer[m+n+1]>>(k+4))&1)|(ega<<2);
							ega |= ega<<1;
							video.screen[l+o]=ega;
						}
					}
					l+=160;
					m+=320;
				}
			}
		}
		while (FrameReady()==0) {}
	}
}

void v_drawscreendirty(dirty_rec_t dirty) {
	int i, j, k;
	if (dirty.w<=0||dirty.h<=0||dirty.sx>=160||dirty.sy>=200)
		return;
	/*if (video.mode==MODE_CGA) {
	} else {*/
		k = (dirty.sy*160)+dirty.sx;
		for (j=0; j<dirty.h; j++) {
			for (i=0; i<dirty.w; i++) {
				video.back_buffer[k+i] = video.scrn_buffer[k+i];
			}
			k+=160;
		}
	//}
}

void v_drawbackbufferdirty(dirty_rec_t dirty) {
	unsigned short offsets[4] = {0,8192,16384,24576};
	unsigned char ega;
	int i, j, k, l, m;
	if (dirty.w<=0||dirty.h<=0||dirty.sx>=160||dirty.sy>=200)
		return;
	/*if (video.mode==MODE_CGA) {
	} else*/ if (video.mode==MODE_TGA) {
		k = dirty.sy&3;
		l = ((dirty.sy>>2)*160)+dirty.sx;
		m = (dirty.sy*160)+dirty.sx;
		for (j=0; j<dirty.h; j++) {
			for (i=0; i<dirty.w; i++) {
				video.screen[offsets[k]+l+i] = video.back_buffer[m+i];
			}
			k=(k+1)&3;
			if (k==0)
				l+=160;
			m+=160;
		}
	} else {
		dirty.w+=dirty.sx&3;
		outp(0x03C4,0x02);
		m = (dirty.sy*160)+(dirty.sx&0xFFFC);
		l = (dirty.sy*40)+(dirty.sx>>2);
		for (j=0; j<dirty.h; j++) {
			for (i=0; i<4; i++) {
				outp(0x03C5,ega_filter[i]);
				for (k=0; k<dirty.w; k+=4) {
					ega = (video.back_buffer[m+k]>>i)&1;
					ega = ((video.back_buffer[m+k+1]>>i)&1)|(ega<<2);
					ega = ((video.back_buffer[m+k+2]>>i)&1)|(ega<<2);
					ega = ((video.back_buffer[m+k+3]>>i)&1)|(ega<<2);
					ega |= ega<<1;
					video.screen[l+(k>>2)]=ega;
				}
			}
			l+=40;
			m+=160;
		}
	}
}

typedef struct {
	ImageHeader_t *img, *msk;
	short sx, sy;
	short lx, ly;
	short w, h;
} spriteblocker_t;
spriteblocker_t spr_getblock(int seq, int idx, int x, int y);
void v_drawimage(short index, short x, short y) {
	short i, j, k;
	unsigned short offset;
	unsigned char *pxls, *p, c, l, m, n, o, r, t;
	spriteblocker_t sb = spr_getblock(0,index,x,y);
	pxls = ((unsigned char*)gorgon.sprites.hdr) + sb.img->Offset;
	if (gorgon.sprites.hdr->MagicNumber==IMGP16_MAGICNUMBER) {
		k = sb.lx&1;
		pxls += (sb.ly * sb.img->Width) + (sb.lx>>1);
		if (video.target_buffer == video.scrn_buffer || video.target_buffer == video.back_buffer) {
			offset = (sb.sy*160) + sb.sx;
			for (i=0; i<sb.h; i++) {
				p = pxls;
				pxls+=sb.img->Width;
				for (j=0; j<sb.w; j++) {
					if (k)
						video.target_buffer[offset+j] = ((*p)&0xF0)|(((*p)&0xF0)>>4);
					else
						video.target_buffer[offset+j] = ((*p)&0x0F)|(((*p)&0x0F)<<4);
					if (k)
						p++;
					k^=1;
				}
				offset+=160;
			}
		} else {
			offset = (sb.sy*80) + (sb.sx>>1);
			t = sb.sx&1;
			m = t?0x0F:0xF0;
			l = k?0xF0:0x0F;
			for (i=0; i<sb.h; i++) {
				p = pxls;
				pxls+=sb.img->Width;
				n = m;
				r = 0;
				o = l;
				for (j=0; j<sb.w; j++) {
					c = (*p)&o;
					o^=0xFF;
					c |= c<<4;
					c |= c>>4;
					video.target_buffer[offset+r] &= n;
					n^=0xFF;
					video.target_buffer[offset+r] |= c&n;
					if (k)
						p++;
					k^=1;
					if (t)
						r++;
					t^=1;
				}
				offset+=80;
			}
		}
	//} else if (gorgon.sprites.hdr->MagicNumber==IMGPCGA_MAGICNUMBER) {
	}
}

void v_setpalette(int index) {
	int i;
	union REGS regs;
	if (palette.hdr==NULL)
		return;
	if (index<0 || index>=palette.hdr->Count)
		return;
	if (video.mode==MODE_CGA) {
		regs.h.ah=0x0B;
		regs.x.bx=palette.data[index].hilo;
		int86(0x10,&regs,&regs);
	} else {
		outp(0x03C8,0);
		for (i=0; i<8; i++) {
			outp(0x03C9,palette.data[index].r[i]>>2);
			outp(0x03C9,palette.data[index].g[i]>>2);
			outp(0x03C9,palette.data[index].b[i]>>2);
		}
		outp(0x03C8,16);
		for (i=8; i<16; i++) {
			outp(0x03C9,palette.data[index].r[i]>>2);
			outp(0x03C9,palette.data[index].g[i]>>2);
			outp(0x03C9,palette.data[index].b[i]>>2);
		}
	}
}

void v_movecursor(int x, int y) {
	union REGS regs;
	regs.h.ah=2;
	regs.h.bh=0;
	regs.h.dh=(unsigned char)y;
	regs.h.dl=(unsigned char)x;
	int86(0x10,&regs,&regs);
}

extern dirty_rec_t cleanuprec;
void v_refresh() {
	v_drawbackbufferdirty(cleanuprec);
	cleanuprec.sx=0;
	cleanuprec.sy=0;
	cleanuprec.w=0;
	cleanuprec.h=0;
}

void v_drawwindow(int x, int y, int w, int h) {
	int i, j;
	if (x<0) {
		w+=x;
		x=0;
	}
	if ((x+w)>40)
		w = 40-x;
	if (y<0) {
		h+=y;
		y=0;
	}
	if ((y+h)>25)
		w = 25-x;
	cleanuprec.sx=x<<2;
	cleanuprec.w=(w+1)<<2;
	cleanuprec.sy=y<<3;
	cleanuprec.h=(h+1)<<3;
	printchar(0,x,y,128);
	for (j=1; j<w; j++)
		printchar(0,x+j,y,131);
	printchar(0,x+w,y,129);
	for (i=1; i<h; i++) {
		printchar(0,x,y+i,130);
		for (j=1; j<w; j++) {
			printchar(0,x+j,y+i,32);
		}
		printchar(0,x+w,y+i,130);
	}
	printchar(0,x,y+h,129);
	for (j=1; j<w; j++) {
		printchar(0,x+j,y+h,131);
	}
	printchar(0,x+w,y+h,128);
}

/*****************************************
	Text Printer Functions
*****************************************/
struct {
	char fg, bk;
} fontdata;
void settextcolors(char fg, char bk) {
	fontdata.fg = fg&0x0F;
	fontdata.bk = bk&0x0F;
	if (video.mode==MODE_CGA) {
		fontdata.fg&=3;
		fontdata.fg|=fontdata.fg<<2;
		fontdata.bk&=3;
		fontdata.bk|=fontdata.bk<<2;
	} else {
		fontdata.fg&=15;
		fontdata.fg|=fontdata.fg<<4;
		fontdata.bk&=15;
		fontdata.bk|=fontdata.bk<<4;
	}
}

void print(short fnt, short x, short y, char *fmt, ...) {
	char buffer[1024], *ptr;
	short sx=x, sy=y;
	va_list args;
	va_start(args,fmt);
	vsprintf(buffer,fmt,args);
	va_end(args);
	ptr = buffer;
	while(*ptr!=0) {
		if (*ptr>=32) {
			printchar(fnt,sx,sy,*ptr);
			sx++;
		} else if (*ptr==0x0D)
			sx=x;
		else if (*ptr=='\t')
			sx+=4;
		else if (*ptr==0x0A) {
			sx=x;
			sy++;
		}
		if (scr_slowtype()) {
			while (!FrameReady()) {}
		}
		ptr++;
	}
}

void printchar(short fnt, short x, short y, short c) {
	short i, j, off;
	unsigned char tgamask[4]={0,0xF0,0x0F,0xFF};
	unsigned short offsets[4] = {0,0x2000,0x4000,0x6000};
	unsigned char egamask[2] = {0,0xFF};
	unsigned char *mask, m, *screen;
	ImageHeader_t *img;
	c-=32;
	if (fnt<0||fnt>3||c<0||c>gorgon.fonts[fnt].hdr->ImageCount)
		return;
	img	= &gorgon.fonts[fnt].imgs[c];
	mask = (unsigned char*)gorgon.fonts[fnt].hdr + img->Offset;
	screen = video.screen;
	/*if (video.mode==MODE_CGA) {
	} else*/ if (video.mode==MODE_TGA) {
		screen+=((y<<1)*160)+(x<<2);
		for (i=0;i<8;i++) {
			m = mask[i];
			for (j=0;j<4;j++) {
				screen[offsets[i&3]+j] = (tgamask[m&3]&fontdata.fg)|(tgamask[(~m)&3]&fontdata.bk);
				m>>=2;
			}
			if (i==3)
				screen+=160;
		}
	} else {
		screen+=(y*320)+x;
		outp(0x03C4,0x02);
		for (j=0;j<4;j++) {
			off = 0;
			outp(0x03C5,1<<j);
			for (i=0;i<8;i++) {
				m = mask[i]&egamask[(fontdata.fg>>j)&1];
				m |= ~mask[i]&egamask[(fontdata.bk>>j)&1];
				m = ((m&0x0F)<<4)|((m&0xF0)>>4);
				m = ((m&0x33)<<2)|((m&0xCC)>>2);
				m = ((m&0x55)<<1)|((m&0xAA)>>1);
				screen[off]=m;
				off+=40;
			}
		}
	}
}
