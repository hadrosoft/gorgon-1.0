#include <alloc.h>
#include <dos.h>

#include "gorgon.h"

extern char *sys_ver;

typedef void (interrupt far* INTFUNCPTR)(void);
struct {
	INTFUNCPTR oldTimerInterrupt;
	long int milliseconds;
	int frequency;
} pit;

#define SOUNDPACK_MAGICNUMBER	0x20646E53
#define PICKLE_MAGICNUMBER		0x6C6B504E

struct {
	pheader_t *hdr;
	psong_t *songs;
	unsigned char *track_start;
	unsigned char *track_pos;
	unsigned char *track_end;
	unsigned short duration;
	unsigned short time;
	unsigned short framelength;
	unsigned short framecount;
	char playing;
	char loopme;
	char dev;
} pickle = {0};

unsigned short tandy_notes[16] = {
	6727,	//C0  - 110000/16.351875
	6350,	//Db0 - 110000/17.32375
	5993,	//D0  - 110000/18.35375
	5657,	//Eb0 - 110000/19.445625
	5339,	//E0  - 110000/20.601875
	5040,	//F0  - 110000/21.826875
	4757,	//Gb0  - 110000/23.124375
	4490,	//G0  - 110000/24.5
	4238,	//Ab0  - 110000/25.95625
	4000,	//A0 - 110000/27.5
	3776,	//Bb0  - 110000/29.135
	3564,	//B0  - 110000/30.8675
};
unsigned short pc_notes[16] = {
	262,	//C4
	277,	//Db4
	294,	//D4
	311,	//Eb4
	330,	//E4
	349,	//F4
	370,	//Gb4
	392,	//G4
	415,	//Ab4
	440,	//A4
	466,	//Bb4
	494,	//B4
};
void p_playframe_tandy() {
	int i;
	unsigned short n;
	unsigned char mask = *pickle.track_pos;
	pickle.track_pos++;
	if (mask==0)
		return;
	for (i=0; i<3; i++) {
		if (mask&(1<<i)) {
			n = tandy_notes[pickle.track_pos[1]&0x0F] >> (pickle.track_pos[1]>>4);
			outportb(0xC0,0x90|(i<<5)|(pickle.track_pos[0]&0x0F));	// 0x90|(channel<<5)|(volume&0x0F)
			outportb(0xC0,0x80|(i<<5)|(n&0x0F));	// 0x80|(channel<<5)|(note&0x0F)
			outportb(0xC0,(n>>4)&0x3F);	// (note>>4)&0x3F
			pickle.track_pos+=2;
		}
	}
	if (mask&0x08) {
		if (pickle.track_pos[1]<0x30) {
			n = pc_notes[pickle.track_pos[1]&0x0F] >> (4 - (pickle.track_pos[1]>>4));
			if ((pickle.track_pos[0]&0x0F)<8)
				sound(n);
			else
				nosound();
			outportb(0xC0,0xFF);
		} else {
			nosound();
			outportb(0xC0,0xF0|(pickle.track_pos[0]&0x0F));
			outportb(0xC0,0xE0|(pickle.track_pos[1]>>4));
			outportb(0xC0,0);
		}
		pickle.track_pos+=2;
	}
	for (i=4; i<8; i++) {
		if (mask&(1<<i))
			pickle.track_pos+=2;
	}
}

#define MPU_StatPort	0x331
#define MPU_DataPort	0x330
void p_mt32_program(long int address, int length, unsigned char *msg) {
	int i, c;
	unsigned char forward[8] = {0xF0,0x41,0x10,0x16,0x12,0x00,0x00,0x00};
	forward[5] = address>>16;
	forward[6] = address>>8;
	forward[7] = address;
	c = 0x80;
	for (i=0; i<8; i++) {
		outportb(MPU_DataPort,forward[i]);
		if (i>4) {
			c -= forward[i];
			if (c<0)
				c+=0x80;
		}
	}
	for (i=0; i<length; i++) {
		outportb(MPU_DataPort,msg[i]);
		c -= msg[i];
		if (c<0)
			c+=0x80;
	}
	outportb(MPU_DataPort,(char)(c&0x7F));
	outportb(MPU_DataPort,0xF7);
}

unsigned char midi_notes[4] = {0};
unsigned char midi_vols[4] = {0};
unsigned char midi_octaves[16] = {0,12,24,36,48,60,72,84,0};
void p_playframe_midi() {
	int i, n, v;
	unsigned short frame[8];
	unsigned char mask = *pickle.track_pos;
	pickle.track_pos++;
	for (i=0; i<8; i++) {
		if (mask&(1<<i)) {
			frame[i] = *((unsigned short*)pickle.track_pos);
			pickle.track_pos+=2;
		}
	}
	for (i=7; i>=0; i--) {
		if (mask&(1<<i)) {
			if (i<4) {
				n = ((frame[i]>>8)&0x0F);
				n += midi_octaves[frame[i]>>12];
				if (n!=midi_notes[i]) {
					//Turn off previous note
					outportb(MPU_DataPort,0x80|(i+1));
					outportb(MPU_DataPort,midi_notes[i]);
					outportb(MPU_DataPort,0);
					//Turn on new note
					outportb(MPU_DataPort,0x90|(i+1));
					outportb(MPU_DataPort,n);
					outportb(MPU_DataPort,40);
					midi_notes[i] = n;
				}
				v = (16 - (frame[i]&0x0F)) << 3;
				v -= 1;
				v &= 0x7F;
				if (v!=midi_vols[i]) {
					outportb(MPU_DataPort,0xB0|(i+1));
					outportb(MPU_DataPort,0x07);
					outportb(MPU_DataPort,v);
					outportb(MPU_DataPort,0x27);
					outportb(MPU_DataPort,v);
					midi_vols[i]=v;
				}
				if (v==0)
					midi_notes[i] = 255;
			} else if (i<8) {
				outportb(MPU_DataPort,0xC0|(i-3));
				outportb(MPU_DataPort,(char)(frame[i]>>8));
			}
		}
	}
}

typedef void (*PicklePlayer)();
PicklePlayer pickle_players[2] = {
	p_playframe_tandy,
	p_playframe_midi
};

void (*triggercallback)();
unsigned short timer_delay;
unsigned char p_state;
unsigned short tick_counter = 0;
void interrupt far timerHandler(void) {
	static unsigned long count = 0;
	++pit.milliseconds;
	++tick_counter;
	disable();
	if (pickle.playing && pickle.dev>=0) {
		if (pickle.framecount==0) {
			pickle.framecount=pickle.framelength;
			pickle.time++;
			if (pickle.time>pickle.duration) {
				if (pickle.loopme) {
					pickle.time=0;
					pickle.framecount=0;
					pickle.track_pos=pickle.track_start;
				} else
					p_stop();
			} else {
				pickle_players[pickle.dev]();
				/*while (pickle.track_pos[0]!=0x0FF) {
					outportb(0x0C0,pickle.track_pos[0]);
					pickle.track_pos++;
				}*/
				//pickle.track_pos++;
			}
		} else
			pickle.framecount--;
	}
	if (p_state&PICKLE_TIMER) {
		timer_delay--;
		if (timer_delay==0) {
			triggercallback();
			p_state&=~PICKLE_TIMER;
		}
	}
	if (tick_counter>28) {
		tick_counter-=28;
		SetStepTick();
	}
	enable();
	count+=pit.frequency;
	if (count >= 65536) {
		count-=65536;
		pit.oldTimerInterrupt();
	} else
		outp(0x20,0x20);
}

void timer_init(short freq) {
	pit.frequency = freq;
	disable();
	pit.oldTimerInterrupt = getvect(8);
	pit.milliseconds = 0;
	setvect(8,timerHandler);
	outp(0x43,0x36);
	outp(0x40,(unsigned char)(freq&0xff));
	outp(0x40,(unsigned char)((freq>>8)&0xff));
	enable();
}

void timer_deinit() {
	disable();
	setvect(8,pit.oldTimerInterrupt);
	outp(0x43,0x36);
	outp(0x40,0x00);
	outp(0x40,0x00);
	enable();
	pit.oldTimerInterrupt = NULL;
}

char p_init(short device) {
	char msg[21] = " * Gorgon         * ";
	int c1 = 0, c2;
	pickle.dev = device;
	pit.oldTimerInterrupt = 0;
	p_state = 0;
	timer_init(1103);
	if (device>0) {
		while(inportb(MPU_StatPort)&0x40) {//output ready
			c1++;
			if (c1>200)
				return 0;
		}
		outportb(MPU_StatPort,0xFF);//Send Reset Signal
		c1 = 0;
		do {
			c2 = 0;
			while(inportb(MPU_StatPort)&0x80) {//input ready
				c2++;
				if (c2>200)
					return 0;
			}
			c1++;
			if (c1>200)
				return 0;
		} while(inportb(MPU_DataPort)!=0xFE);
		c1 = 0;
		while(inportb(MPU_StatPort)&0x40) {//output ready
			c1++;
			if (c1>200)
				return 0;
		}
		outportb(MPU_StatPort,0x3F);
		p_mt32_program(0x7F0000,0,NULL);
		sprintf(msg," * Gorgon  %6s * ",sys_ver);
		p_mt32_program(0x200000,20,msg);
	}
	return 1;
}

void p_quit() {
	int c1 = 0;
	p_stop();
	if (pit.oldTimerInterrupt!=NULL)
		timer_deinit();
	if (pickle.dev>0) {
		while(inportb(MPU_StatPort)&0x40) {//output ready
			c1++;
			if (c1>200)
				return;
		}
		outportb(MPU_StatPort,0xFF);//Send Reset Signal
	}
}

void p_settimercallback(void (*callback)()) {
	triggercallback = callback;
}

void p_settimer(unsigned short seconds, unsigned short milliseconds) {
	p_state|=PICKLE_TIMER;
	timer_delay = (seconds*1000)+milliseconds;
}

void p_cleartimer() {
	p_state&=~PICKLE_TIMER;
}

bool p_loadfile(const char *filename) {
	unsigned char *fbuffer;
	size_t fsize;
	if (pickle.dev<0)
		return FALSE;
	fbuffer = BlockLoad(filename,&fsize);
	if (fbuffer==NULL)
		return FALSE;
	pickle.hdr = (pheader_t*)fbuffer;
	if (pickle.hdr->MagicNumber!=PICKLE_MAGICNUMBER) {
		err_SetError(ERR_INVALIDFILE,"Invalid file '%s'!",filename);
		free(fbuffer);
		return FALSE;
	}
	if (pickle.hdr->Version!=1) {
		err_SetError(ERR_INVALIDFILE,"Unsupported file version '%s'!",filename);
		free(fbuffer);
		return FALSE;
	}
	pickle.songs = (psong_t*)(fbuffer + pickle.hdr->TableOffset);
	return TRUE;
}

void p_unloadfile() {
	if (pickle.hdr!=NULL)
		free(pickle.hdr);
	pickle.hdr = NULL;
	pickle.songs = NULL;
}

void p_play(unsigned short idx) {
	if (pickle.dev<0)
		return;
	if (idx>=pickle.hdr->SongCount) {
		pickle.playing = FALSE;
		err_SetError(0xFF,"Invalid track ID!");
	} else {
		pickle.track_start = ((unsigned char*)pickle.hdr) + pickle.songs[idx].offset;
		pickle.track_pos = pickle.track_start;
		pickle.track_end = pickle.track_start + pickle.songs[idx].bytelength;
		pickle.framelength = pickle.songs[idx].frequency;
		pickle.framecount = 0;
		pickle.duration = pickle.songs[idx].duration;
		pickle.time = 0;
		pickle.playing = TRUE;
	}
}

void p_stop() {
	int i;
	if (pickle.dev<0)
		return;
	if (pickle.dev==0) {
		nosound();
		outportb(0x0C0,0x9F);
		outportb(0x0C0,0x9F|0x20);
		outportb(0x0C0,0x9F|0x40);
		outportb(0x0C0,0x9F|0x60);
	} else {
		for (i=0; i<4; i++) {
			outportb(MPU_DataPort,0x80|(i+1));
			outportb(MPU_DataPort,midi_notes[i]);
			outportb(MPU_DataPort,0);
			midi_notes[i] = 0;
			midi_vols[i] = 0;
			/*outp(MPU_DataPort,0xB0|i);
			outp(MPU_DataPort,0x78);
			outp(MPU_DataPort,0);*/
		}
	}
	pickle.playing = FALSE;
}

void p_loopplay(bool state) {
	pickle.loopme = state;
}

bool p_testtimer(unsigned long int value) {
	bool r = pit.milliseconds>=value;
	if (r)
		pit.milliseconds-=value;
	return r;
}
