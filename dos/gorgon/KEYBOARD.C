#include <dos.h>

static void (interrupt far *oldkb)(void);

#define NUM_SCAN_QUEUE	256
struct {
	unsigned short queue[NUM_SCAN_QUEUE];
	unsigned char escaped;
	unsigned char head;
	unsigned char tail;
} kb;

unsigned char kbscan;
static void interrupt far get_scan(void) {
	unsigned short prev = kbscan;
	unsigned short t;
	disable();
	kbscan = inp(0x60);
	t = inp(0x61);
	outp(0x61,t|0x80);
	outp(0x61,t);
	outp(0x20,0x20);
	enable();
	if (kbscan != prev) {
		if (kbscan >= 0xE0) {
			*(kb.queue+kb.tail) = (unsigned short)kbscan << 8;
			kb.escaped = 1;
		} else {
			if(kb.escaped==0)
				*(kb.queue+kb.tail) = 0;
			*(kb.queue+kb.tail) += kbscan;
			++kb.tail;
			kb.escaped = 0;
		}
	}
}

void kb_init() {
	unsigned char far *bios_key_state;
	kb.escaped=0;
	kb.head=0;
	kb.tail=0;
	kbscan=0;
	oldkb = getvect(9);
	bios_key_state=(unsigned char far*)MK_FP(0x040,0x017);
	*bios_key_state&=(~(32|64));
	disable();
	setvect(9,get_scan);
	enable();
}

void kb_quit() {
	setvect(9,oldkb);
}
