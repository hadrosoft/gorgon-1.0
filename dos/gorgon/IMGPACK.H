#ifndef _IMG_PACK_H
#define _IMG_PACK_H

#define IMGPACKR_MAGICNUMBER	0x8992
#define IMGP16_MAGICNUMBER		0x8993
#define IMGPEGA_MAGICNUMBER		0x8994
#define IMGPCGA_MAGICNUMBER		0x8995
#define IMGPMCA_MAGICNUMBER		0x8996

#ifndef bounds_t
typedef struct {
	unsigned short left;
	unsigned short right;
	unsigned short top;
	unsigned short bottom;
} bounds_t;
#endif

#ifndef vector2_t
typedef struct {
	unsigned short x, y;
} vector2_t;
#endif

typedef struct {
	unsigned short MagicNumber;
	unsigned char PalettesCount;
	unsigned char SequenceCount;
	unsigned short ImageCount;
	unsigned char Reserved[2];
	unsigned long int PalettesOffset;
	unsigned long int SequenceOffset;
	unsigned long int ImagesOffset;
} ImagePackHeader_t;

typedef struct {
	unsigned long int Offset;
	unsigned short Width, Height;
	short OriginX, OriginY;
	short HotSpotX, HotSpotY;
} ImageHeader_t;

typedef struct {
	unsigned short FirstFrame;
	unsigned short Length;
} ImageSequence_t;

typedef struct {
	unsigned char Index;
	unsigned char Width;
	unsigned short Offset;
} ImagePalette_t;

typedef struct {
	ImagePackHeader_t *hdr;
	ImageHeader_t *imgs;
	ImageSequence_t *seqs;
	ImagePalette_t *pals;
} ImagePack_t;

void imp_setbounds(bounds_t newbounds);
bool imp_load(ImagePack_t *pack, const char *filename);
void imp_unload(ImagePack_t *pack);
vector2_t imp_hotspot(ImagePack_t *pack, int index);
#ifdef IMP_VGA
void imp_window(ImagePack_t *pack, int firstid, int left, int right, int top, int bottom, unsigned char colour);
void imp_print(ImagePack_t *pack, int x, int y, unsigned char colour, const char *fmt, ...);
void imp_draw(ImagePack_t *pack, int index, int x, int y, unsigned char colour);
void imp_loadpal(ImagePack_t *pack, int start, int index);
#else
void imp_window(ImagePack_t *pack, int firstid, int left, int right, int top, int bottom);
void imp_print(ImagePack_t *pack, int x, int y,  const char *fmt, ...);
void imp_draw(ImagePack_t *pack, int index, int x, int y);
#endif

#endif
