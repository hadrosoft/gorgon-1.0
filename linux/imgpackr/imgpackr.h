#ifndef IMGPACKR_H
#define IMGPACKR_H

#include "../imagelib/_images.h"

#define IMGPACKR_MAGICNUMBER	0x8992
#define IMGP16_MAGICNUMBER		0x8993
#define IMGPEGA_MAGICNUMBER		0x8994
#define IMGPCGA_MAGICNUMBER		0x8995
#define IMGPMCA_MAGICNUMBER		0x8996

typedef struct {
	uint16_t MagicNumber;
	uint8_t PalettesCount;
	uint8_t SequenceCount;
	uint16_t ImageCount;
	uint8_t Reserved[2];
	uint32_t PalettesOffset;
	uint32_t SequenceOffset;
	uint32_t ImagesOffset;
} ImagePackHeader_t;

typedef struct {
	uint32_t Offset;
	uint16_t Width, Height;
	int16_t OriginX, OriginY;
	int16_t HotSpotX, HotSpotY;
} ImageHeader_t;

typedef struct {
	uint16_t FirstFrame;
	uint16_t Length;
} ImageSequence_t;

typedef struct {
	uint8_t Index;
	uint8_t Width;
	uint16_t Offset;
} ImagePalette_t;

#endif
