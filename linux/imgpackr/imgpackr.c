#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "imgpackr.h"

#define VER_MAJOR			1
#define VER_MINOR			2

#define MAX_IMAGES		1024
#define MAX_SEQUENCES	256

typedef struct {
	unsigned long int x, y;
	unsigned char w, h;
} imagequad_t;

ImagePackHeader_t pack = {IMGP16_MAGICNUMBER,0,0,0,0};
ImageHeader_t hdrs[MAX_IMAGES] = {0};
imagequad_t quads[MAX_IMAGES] = {0};
ImageSequence_t seqs[MAX_SEQUENCES] = {0};
ImagePalette_t pals[16] = {0};
unsigned char palstarts[16] = {0};

enum {
	OUT_VGA16 = 0,
	OUT_VGA256,
	OUT_EGA,
	OUT_TANDY,
	OUT_CGA,
	OUT_MCA
};

int main(int argc, char **argv) {
	image_t *img = NULL;
	char c, buf[100], outname[100], *c1, *c2;
	unsigned char outbuf[256];
	unsigned char *src;
	int i, j, k, vo = OUT_VGA16;
	FILE *f;
	printf("IMage Packer\nWritten by the Lobdegg\nCopyright (c) 2021\n");
	printf("Version %d.%d\n",VER_MAJOR,VER_MINOR);
	if (argc<2) {
		printf("Usage: imgpackr <imgpack definition file>\n");
		printf("Definition file format:\n");
		printf("file <filename>\n\tImage file to import. Accepted formats: PCX, BMP\n");
		printf("img <dimensions>:<origin>:<hotspot>\n");
		printf("\t+Dimensions:\tCoordinates defining the sprite within the source image\n");
		printf("\t\t\t<left>,<top>,<width>,<height>\n");
		printf("\t+Origin:\tWhere the 'center' of the image is\n");
		printf("\t\t\t<origin x>,<origin y>\n");
		printf("\t+Hotspot:\tThe relative location used by special events\n");
		printf("\t\t\t<hotspot x>,<hotspot y>\n");
		return 1;
	}
	for (c=2; c<argc; c++) {
		if (strcmp(argv[c],"-256")==0) {
			vo = OUT_VGA256;
			pack.MagicNumber = IMGPACKR_MAGICNUMBER;
		} else if (strcmp(argv[c],"-ega")==0) {
			vo = OUT_EGA;
			pack.MagicNumber = IMGPEGA_MAGICNUMBER;
		} else if (strcmp(argv[c],"-tandy")==0) {
			vo = OUT_TANDY;
		} else if (strcmp(argv[c],"-cga")==0) {
			vo = OUT_CGA;
			pack.MagicNumber = IMGPCGA_MAGICNUMBER;
		} else if (strcmp(argv[c],"-mca")==0) {
			vo = OUT_MCA;
			pack.MagicNumber = IMGPMCA_MAGICNUMBER;
		}
	}
	f = fopen(argv[1],"rb");
	if (f==NULL) {
		printf("Could not open definition file!\n");
		return 2;
	}
	i = 0;
	memset(buf,0,100);
	memset(outname,0,100);
	while(!feof(f)) {
		c = fgetc(f);
		if (c=='\n'||c=='\r'||c==255) {
			for (i=0; i<100;i++)
				buf[i] = tolower(buf[i]);
			if (strncmp(buf,"file ",5)==0) {
				i = strlen(buf);
				if (strcmp(buf+i-3,"pcx")==0) {
					img = LoadPCX(buf+5);
				} else if (strcmp(buf+i-3,"bmp")==0) {
					img = LoadBMP(buf+5);
				} else if (strcmp(buf+i-3,"gif")==0) {
					img = LoadGIF(buf+5);
				}
			} else if (strncmp(buf,"out ",4)==0) {
				strcpy(outname,buf+4);
			} else if (strncmp(buf,"sequence",8)==0) {
				pack.SequenceCount++;
				seqs[pack.SequenceCount].FirstFrame = pack.ImageCount;
			} else if (strncmp(buf,"img ",4)==0) {
				c1 = buf + 4;
				c2 = strchr(c1,',');
				*c2 = 0;
				quads[pack.ImageCount].x = atoi(c1);
				c1 = c2 + 1;
				c2 = strchr(c1,',');
				*c2 = 0;
				quads[pack.ImageCount].y = atoi(c1);
				c1 = c2 + 1;
				c2 = strchr(c1,',');
				*c2 = 0;
				hdrs[pack.ImageCount].Width = atoi(c1);
				quads[pack.ImageCount].w = hdrs[pack.ImageCount].Width;
				c1 = c2 + 1;
				c2 = strchr(c1,':');
				*c2 = 0;
				hdrs[pack.ImageCount].Height = atoi(c1);
				quads[pack.ImageCount].h = hdrs[pack.ImageCount].Height;
				switch(vo) {
					case OUT_VGA16:
						hdrs[pack.ImageCount].Height>>=1;
						if (quads[pack.ImageCount].h&0x01)
							hdrs[pack.ImageCount].Height++;
						break;
					case OUT_TANDY:
					case OUT_EGA:
						hdrs[pack.ImageCount].Width>>=1;
						if (quads[pack.ImageCount].w&0x01)
							hdrs[pack.ImageCount].Width++;
						break;
					case OUT_CGA:
						hdrs[pack.ImageCount].Width>>=2;
						if (quads[pack.ImageCount].w&0x03)
							hdrs[pack.ImageCount].Width++;
						break;
					case OUT_MCA:
						hdrs[pack.ImageCount].Width>>=3;
						if (quads[pack.ImageCount].w&0x07)
							hdrs[pack.ImageCount].Width++;
						break;
				}
				c1 = c2 + 1;
				c2 = strchr(c1,',');
				*c2 = 0;
				hdrs[pack.ImageCount].OriginX = atoi(c1);
				c1 = c2 + 1;
				c2 = strchr(c1,':');
				*c2 = 0;
				hdrs[pack.ImageCount].OriginY = atoi(c1);
				c1 = c2 + 1;
				c2 = strchr(c1,',');
				*c2 = 0;
				hdrs[pack.ImageCount].HotSpotX = atoi(c1);
				c1 = c2 + 1;
				c2 = strchr(c1,' ');
				if (c2!=NULL)
					*c2 = 0;
				hdrs[pack.ImageCount].HotSpotY = atoi(c1);
				pack.ImageCount++;
				seqs[pack.SequenceCount].Length++;
			} else if (strncmp(buf,"pal ",4)==0) {
				c1 = buf + 4;
				c2 = strchr(c1,':');
				*c2 = 0;
				pals[pack.PalettesCount].Index = atoi(c1);
				c1 = c2 + 1;
				c2 = strchr(c1,':');
				*c2 = 0;
				palstarts[pack.PalettesCount] = atoi(c1);
				c1 = c2 + 1;
				c2 = strchr(c1,' ');
				if (c2!=NULL)
					*c2 = 0;
				pals[pack.PalettesCount].Width = atoi(c1);
				pack.PalettesCount++;
			}
			i = 0;
			memset(buf,0,100);
		} else {
			buf[i++]=c;
		}
	}
	pack.SequenceCount++;
	printf("%d images defined!\n",pack.ImageCount);
	printf("%d palettes defined!\n",pack.PalettesCount);
	fclose(f);
	if (img == NULL) {
		printf("Image file not loaded!\n");
		return 3;
	}
	/*f = fopen("out.bin","wb");
	if (f!=NULL) {
		fwrite(img,1,12,f);
		fwrite(&img->palettelength,1,3,f);
		fwrite(img->buffer,1,img->bufferlength,f);
		fwrite(img->palette,1,img->palettelength,f);
		fclose(f);
	}*/
	pack.PalettesOffset = sizeof(ImagePackHeader_t);
	pack.SequenceOffset = pack.PalettesOffset + (pack.PalettesCount * sizeof(ImagePalette_t));
	for (i=0; i<pack.PalettesCount; i++) {
		pals[i].Offset = (unsigned short)pack.SequenceOffset;
		pack.SequenceOffset += pals[i].Width * 3;
	}
	pack.ImagesOffset = pack.SequenceOffset + (pack.SequenceCount * sizeof(ImageSequence_t));
	j = pack.ImagesOffset + (pack.ImageCount * sizeof(ImageHeader_t));
	for (i=0; i<pack.ImageCount; i++) {
		hdrs[i].Offset = j;
		j += hdrs[i].Width * hdrs[i].Height;
	}
	f = fopen(outname,"wb");
	if (f != NULL) {
		printf("[0]: %x\n",ftell(f));
		fwrite(&pack,1,sizeof(ImagePackHeader_t),f);
		//Write Palette Table
		printf("[1]: %x\n",ftell(f));
		fwrite(pals,1,sizeof(ImagePalette_t)*pack.PalettesCount,f);
		//Write Palette Data
		printf("[2]: %x\n",ftell(f));
		for (i=0; i<pack.PalettesCount; i++) {
			fwrite(img->palette + (palstarts[i]*3),1,pals[i].Width*3,f);
		}
		//Write Sequence Table
		printf("[3]: %x [%x:%x]\n",ftell(f),sizeof(ImageSequence_t),pack.SequenceCount);
		fwrite(seqs,1,sizeof(ImageSequence_t)*pack.SequenceCount,f);
		//Write Image Headers
		printf("[4]: %x [%x:%x]\n",ftell(f),sizeof(ImageHeader_t),pack.ImageCount);
		fwrite(hdrs,1,sizeof(ImageHeader_t)*pack.ImageCount,f);
		//Write Image Data
		printf("[5]: %x\n",ftell(f));
		for (i=0; i<pack.ImageCount; i++) {
			src = img->buffer + (quads[i].y*img->width) + quads[i].x;
			switch(vo) {
				case OUT_VGA16:
					printf("[%d]%dx%d(%d)\n",i,quads[i].w,quads[i].h,hdrs[i].Height);
					for (j=0; j<quads[i].w; j++) {//column major
						memset(outbuf,0xFF,256);
						for (k=0; k<quads[i].h; k++) {
							if (k&1) {
								outbuf[k>>1] &= 0x0F;
								outbuf[k>>1] |= src[(k*img->width)+j] << 4;
							} else {
								outbuf[k>>1] &= 0xF0;
								outbuf[k>>1] |= src[(k*img->width)+j] & 0x0F;
							}
						}
						fwrite(outbuf,1,hdrs[i].Height,f);
					}
					break;
				case OUT_TANDY:
					for (j=0; j<hdrs[i].Height; j++) {//row major
						memset(outbuf,0xFF,256);
						for (k=0; k<quads[i].w; k++) {
							if (k&0x01) {
								outbuf[k>>1] &= 0x0F;
								outbuf[k>>1] |= src[k] << 4;
							} else {
								outbuf[k>>1] &= 0xF0;
								outbuf[k>>1] |= src[k] & 0x0F;
							}
						}
						fwrite(outbuf,1,hdrs[i].Width,f);
						src += img->width;
					}
					break;
				case OUT_VGA256:
					//column major
					for (j=0; j<hdrs[i].Width; j++) {//column major
						memset(outbuf,0xFF,256);
						for (k=0; k<hdrs[i].Height; k++)
							outbuf[k] = src[(k*img->width)+j];
						fwrite(outbuf,1,hdrs[i].Height,f);
					}
					//row major
					/*for (j=0; j<hdrs[i].Height; j++) {
						fwrite(src,1,hdrs[i].Width,f);
						src += img->width;
					}*/
					break;
				case OUT_EGA:
					break;
				case OUT_CGA:
					for (j=0; j<hdrs[i].Height; j++) {//row major
						memset(outbuf,0,256);
						for (k=0; k<quads[i].w; k++) {
							if ((k&0x03) == 3) {
								outbuf[k>>2] &= 0x3F;
								outbuf[k>>2] |= (src[k] & 3) << 6;
							} else if ((k&0x03) == 2) {
								outbuf[k>>2] &= 0xCF;
								outbuf[k>>2] |= (src[k] & 3) << 4;
							} else if ((k&0x03) == 1) {
								outbuf[k>>2] &= 0xF3;
								outbuf[k>>2] |= (src[k] & 3) << 2;
							} else {
								outbuf[k>>2] &= 0xFC;
								outbuf[k>>2] |= (src[k] & 3) << 0;
							}
						}
						fwrite(outbuf,1,hdrs[i].Width,f);
						src += img->width;
					}
					break;
				case OUT_MCA:
					for (j=0; j<hdrs[i].Height; j++) {
						memset(outbuf,0,256);
						for (k=0; k<quads[i].w; k++) {
							outbuf[k>>3] &= ~(1<<(k&7));
							if (src[k]<32)
								outbuf[k>>3] |= 1<<(k&7);
						}
						fwrite(outbuf,1,hdrs[i].Width,f);
						src += img->width;
					}
					break;
			}
		}
		printf("[5]: %x\n",ftell(f));
		fclose(f);
	} else {
		printf("Error opening output file '%s'!\n",outname);
	}
	free(img->buffer);
	free(img->palette);
	free(img);
	return 0;
}
