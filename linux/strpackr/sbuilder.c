#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define VER_MAJOR			1
#define VER_MINOR			2

#define STRING_MAGICNUMBER	0xFA88
typedef struct {
	uint16_t MagicNumber;
	uint16_t StringCount;
} Strings_hdr_t;

typedef struct {
	uint16_t Offset;
} String_t;

#define MAX_STRINGS	1024

int main(int argc, char **argv) {
	FILE *f;
	Strings_hdr_t hdr;
	String_t table[MAX_STRINGS];
	unsigned short foffs[MAX_STRINGS];
	unsigned char *fbuffer, *sptr, *eptr;
	unsigned char outbuf[256];
	int i, j, k, off = 0, crlf = 0, setoff=0;
	size_t f_len;
	printf("String Packer\nWritten by the Lobdegg\nCopyright (c) 2021\n");
	printf("Version %d.%d\n",VER_MAJOR,VER_MINOR);
	if (argc<2) {
		printf("Please name a text file to convert.\n");
		return 1;
	}
	f = fopen(argv[1],"rb");
	if (f==NULL) {
		printf("Could not open %s!\n",argv[1]);
		return 2;
	}
	fseek(f,0,SEEK_END);
	f_len = ftell(f);
	fseek(f,0,SEEK_SET);
	fbuffer = (unsigned char*)malloc(f_len+2);
	if (fbuffer==NULL) {
		printf("Not enough memory!\n");
		fclose(f);
		return 3;
	}
	fread(fbuffer,1,f_len,f);
	fclose(f);
	fbuffer[f_len]=255;
	fbuffer[f_len+1]=0;
	//printf("contents:\n%s\n",fbuffer);
	eptr = fbuffer + f_len;
	hdr.MagicNumber = STRING_MAGICNUMBER;
	hdr.StringCount = 0;
	table[0].Offset = 0;
	foffs[0] = 0;
	for (i=0; i<(f_len+1); i++) {
		if (fbuffer[i]==0x0A||fbuffer[i]==255) {
			fbuffer[i] = 0;
			if (crlf==1) {
				crlf=0;
			} else {
				crlf=1;
				off++;
				hdr.StringCount++;
				printf("%d - %d - %d\n",hdr.StringCount, off, i);
				table[hdr.StringCount].Offset = off;
				setoff=1;
			}
		} else if (fbuffer[i]==0x0D) {
			fbuffer[i] = 0;
		} else {
			crlf=0;
			if (setoff==1) {
				foffs[hdr.StringCount] = i;
				setoff=0;
			}
			off++;
		}
	}
	printf("%d strings packed!\n",hdr.StringCount);
	off = sizeof(hdr) + (hdr.StringCount * sizeof(String_t));
	for (i=0; i<hdr.StringCount; i++)
		table[i].Offset += off;
	memset(outbuf,0,256);
	strcpy(outbuf,argv[1]);
	sptr = outbuf + strlen(outbuf);
	while (sptr>outbuf) {
		if (*sptr=='.')
			break;
		sptr--;
	}
	strcpy(sptr,".lng");
	f = fopen(outbuf,"wb");
	if (f==NULL) {
		printf("Could not create file %s!\n", outbuf);
		return 4;
	}
	fwrite(&hdr,1,sizeof(hdr),f);
	fwrite(table,1,hdr.StringCount * sizeof(String_t),f);
	k = 0;
	for (i=0; i<hdr.StringCount; i++) {
		memset(outbuf,0,256);
		sptr = fbuffer + foffs[i];
		while (sptr<eptr) {
			if (*sptr==0x5C && sptr[1]==0x6E) {
				fputc(0x0D,f);
				fputc(0x0A,f);
				sptr++;
			} else if (*sptr==0x5C && sptr[1]==0x74) {
				fputc(0x09,f);
				sptr++;
				for (j=i+1; j<hdr.StringCount; j++)
					table[j].Offset--;
				k++;
			} else if (*sptr==0 || *sptr==0x0A || *sptr==0x0D) {
				fputc(0,f);
				break;
			} else {
				fputc(*sptr,f);
			}
			sptr++;
		};
	}
	fputc(0,f);
	if (k!=0) {
		fseek(f,sizeof(hdr),SEEK_SET);
		fwrite(table,1,hdr.StringCount * sizeof(String_t),f);
	}
	fclose(f);
	return 0;
}
