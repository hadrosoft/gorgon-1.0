#include <ctype.h>
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define VER_MAJOR			1
#define VER_MINOR			4

#define MAGICNUMBER	0x7352
#define MAX_ENTRIES	1024

typedef struct {
	uint16_t MagicNumber;
	uint16_t FileCount;
} resource_header_t;

typedef struct {
	char Filename[12];
	uint32_t Offset;
	uint32_t Size;
} resource_entry_t;

resource_header_t hdr = {MAGICNUMBER,0};
resource_entry_t entries[255];
int entry_dirs[255];

int main(int argc, char **argv) {
	size_t i, j;
	char buffer[256], *ptr1, *ptr2;
	FILE *in, *out;
	printf("Resource Packer\nWritten by the Lobdegg\nCopyright (c) 2021\n");
	printf("Version %d.%d\n",VER_MAJOR,VER_MINOR);
	if (argc<3) {
		printf("Please specify both a destination file and at least one file to pack!\n");
		printf("usage: respackr <destination file> <files to pack(use * for wildcards)>\n");
		printf("Maximum permitted asset size: 64000 bytes\n");
		return 1;
	}
	
	memset(entries,0,sizeof(resource_entry_t)*255);
	memset(buffer,0,256);
	printf("Discovering files...\n");
	for (i=2; i<argc; i++) {
		in = fopen(argv[i],"r");
		if (in!=NULL) {
			fseek(in,0,SEEK_END);
			j = ftell(in);
			if (j<64000) {
				entries[hdr.FileCount].Size = j;
				ptr2 = argv[i];
				while(ptr2!=NULL) {
					ptr1 = ptr2;
					ptr2 = strchr(ptr1+1,'/');
				}
				if (*ptr1 == '/') ptr1++;
				printf("::%s\n",argv[i]);
				for (j=0; j<12; j++) {
					if (ptr1[j]==0) break;
					entries[hdr.FileCount].Filename[j] = toupper(ptr1[j]);
				}
				if (hdr.FileCount>0)
					entries[hdr.FileCount].Offset = entries[hdr.FileCount-1].Offset + entries[hdr.FileCount-1].Size;
				entry_dirs[hdr.FileCount] = i;
				memcpy(buffer,entries[hdr.FileCount].Filename,12);
				printf("entrie[%d] = {%s, %08X : %08X}\n",hdr.FileCount,buffer, entries[hdr.FileCount].Offset, entries[hdr.FileCount].Size);
				hdr.FileCount++;
			} else
				printf("%s is too large! Bypassing file.\n",argv[i]);
			fclose(in);
		}
	}
	j = sizeof(resource_header_t) + (sizeof(resource_entry_t) * hdr.FileCount);
	printf("Data offset: %08X\n",j);
	//Adjust offsets by file entry table
	for (i=0; i<hdr.FileCount; i++)
		entries[i].Offset += j;
	//Create our resource file
	out = fopen(argv[1],"wb");
	if (out==NULL) {
		printf("Could not create '%s'!\n",argv[1]);
		return 2;
	}
	//Write table
	fwrite(&hdr,1,sizeof(resource_header_t),out);
	fwrite(entries,1,sizeof(resource_entry_t)*hdr.FileCount,out);
	//Start packing!
	for (i=0; i<hdr.FileCount; i++) {
		in = fopen(argv[entry_dirs[i]],"rb");
		if (in==NULL) {
			printf("Critical error! Could not open '%s'!\n",buffer);
			fclose(out);
			return 3;
		}
		for (j=0; j<entries[i].Size; j++)
			fputc(fgetc(in),out);
		fclose(in);
	}
	//wrap up!
	fclose(out);
	printf("Packed %d files into '%s'.\n",hdr.FileCount,argv[1]);
	return 0;
}
