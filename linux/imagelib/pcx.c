#include <malloc.h>
#include <stdio.h>
#include <string.h>

#include "_images.h"

typedef struct {
	unsigned char Manufacturer;
	unsigned char Version;
	unsigned char EncodingScheme;
	unsigned char BPP;
	unsigned short Left;
	unsigned short Top;
	unsigned short Right;
	unsigned short Bottom;
	unsigned short HResolution;
	unsigned short VResolution;
	unsigned char Palette16[48];
	unsigned char Reserved1;
	unsigned char ColorPlanes;
	unsigned short BytesPerScanline;
	unsigned short PaletteInfo;
	unsigned short HScreenSize;
	unsigned short VScreenSize;
	unsigned char Reserved2[54];
} PCXHeader_t;

image_t *LoadPCX(const char *filename) {
	long int l, dist, scanlength;
	image_t *r;
	size_t eoi, eof;
	unsigned char *scanlinebuf;
	unsigned char *spixels;
	unsigned char a, b;
	PCXHeader_t hdr;
	FILE *f = fopen(filename,"rb");
	if (f == NULL) {
		printf("Error: Failed to open file!\n");
		return NULL;
	}
	fread(&hdr,1,sizeof(PCXHeader_t),f);
	if (hdr.Manufacturer != 0x0a) {
		fclose(f);
		printf("Error: Not valid PCX file!\n");
		return NULL;
	}
	r = (image_t*)malloc(sizeof(image_t));
	scanlength = (unsigned long int)hdr.ColorPlanes * hdr.BytesPerScanline;
	scanlinebuf = (unsigned char*)malloc(scanlength);
	r->width = hdr.Right - hdr.Left + 1;
	printf("%ld\n",r->width);
	r->height = hdr.Bottom - hdr.Top + 1;
	printf("%ld\n",r->height);
	r->bpp = hdr.BPP;
	printf("%d:%d\n",r->bpp,r->bpp>>3);
	r->bufferlength = r->width * (unsigned long int)r->height;// * (r->bpp >> 3);
	printf("%ld:%d\n",r->bufferlength,sizeof(unsigned long int));
	r->buffer = (unsigned char*)malloc(r->bufferlength);
	fseek(f,0x080, SEEK_SET);
	printf("processing pcx %dx%dx%d\n",r->width,r->height,r->bpp);
	dist = 0;
	l = 0;
	printf("%dx%d\n",scanlength,r->height);
	eoi = ftell(f);
	while(l<r->height) {
		a = (unsigned char)fgetc(f);
		if (a>=0xC0) {
			b = fgetc(f);
			memset(scanlinebuf+dist,b,a&0x3F);
			dist+=a&0x3F;
			a = b;
		} else {
			scanlinebuf[dist]=a;
			dist++;
		}
		if (dist>=scanlength) {
			memcpy(r->buffer + (l * r->width),scanlinebuf,r->width);
			dist = 0;
			printf(".");
			l++;
		}
	}
	printf("%x\n%d bytes read!\n",a,ftell(f)-eoi);
	a = fgetc(f);
	free(scanlinebuf);
	if (!feof(f) && (a == 0x0C)) {
		printf("\n256 Color Palette!");
		eoi = ftell(f);
		fseek(f,0,SEEK_END);
		eof = ftell(f);
		fseek(f,eoi,SEEK_SET);
		r->palettelength = eof - eoi;
		r->palette = (unsigned char*)malloc(r->palettelength);
		fread(r->palette,1,r->palettelength,f);
	} else {
		printf("\n16 Color Palette");
		r->palettelength = 48;
		r->palette = (unsigned char*)malloc(48);
		memcpy(r->palette,hdr.Palette16,48);
	}
	printf("\n");
	fclose(f);
	/*f = fopen("pcx.txt","wb");
	if (f!=NULL) {
		for (l=0;l<r->bufferlength;l++) {
			if ((l&0x0F)==0)
				fprintf(f,"\n%04x - ",l);
			fprintf(f,"%02X ",r->buffer[l]);
		}
		fclose(f);
	}*/
	return r;
}
