#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "_images.h"

#define BMP_MAGIC_NUMBER	0x4D42
typedef struct {
	unsigned short MagicNumber;
	unsigned long Size;
	unsigned short Reserved[2];
	unsigned long Offset;
} BMPHeader_t;

typedef struct {
	unsigned long HeaderSize;
	unsigned long Width;
	unsigned long Height;
	unsigned short ColorPlanes;
	unsigned short BPP;
	unsigned long Compression;
	unsigned long ImageSize;//0 on BI_RGB images
	unsigned long HSize;
	unsigned long VSize;
	unsigned long PalSize;
	unsigned long Important;//Not Important
} DIBHeader_t;

image_t *LoadBMP(const char *filename) {
	BMPHeader_t hdr;
	DIBHeader_t dib;
	image_t *r;
	unsigned char paldata[4];
	FILE *f;
	long i, count, p_idx;
	printf("Loading BMP data...\n");
	f = fopen(filename,"rb");
	if (f == NULL) {
		printf("Error: Failed to open file!\n");
		return NULL;
	}
	fread(&hdr,1,sizeof(BMPHeader_t),f);
	if (hdr.MagicNumber!=BMP_MAGIC_NUMBER) {
		fclose(f);
		printf("Error: Not valid BMP file!\n");
		return NULL;
	}
	fread(&dib,1,sizeof(DIBHeader_t),f);
	if (dib.HeaderSize!=40) {
		fclose(f);
		printf("Error: Unsupported BMP format! Please use the Windows 3.1 compliant format.\n");
		return NULL;
	}
	if (dib.BPP>8) {
		fclose(f);
		printf("Error: Only 256 color or less supported!\n");
		return NULL;
	}
	r = (image_t*)malloc(sizeof(image_t));
	if (r==NULL) {
		fclose(f);
		printf("Error: Could not create image structure!\n");
		return NULL;
	}
	memset(r,0,sizeof(image_t));
	r->bufferlength = dib.Width*dib.Height;
	r->width = dib.Width;
	r->height = dib.Height;
	r->buffer = (unsigned char*)malloc(r->bufferlength);
	if (r->buffer==NULL) {
		fclose(f);
		printf("Error: Insufficient memory for pixels!\n");
		return NULL;
	}
	r->palette = (unsigned char*)malloc(768);
	if (r->palette==NULL) {
		free(r->buffer);
		fclose(f);
		printf("Error: Insufficient memory for palette!\n");
		return NULL;
	}
	memset(r->palette,0,768);
	if (dib.PalSize>0)
		count = dib.PalSize;
	else
		count = dib.BPP<<5;
	p_idx = 0;
	printf("Importing palette...\n");
	for (i=0;i<count;i++) {
		fread(paldata,4,1,f);
		r->palette[p_idx+0] = paldata[2];
		r->palette[p_idx+1] = paldata[1];
		r->palette[p_idx+2] = paldata[0];
		p_idx+=3;
	}
	printf("Palette imported with %d entries!\n",i);
	r->bpp = dib.BPP;
	fseek(f,hdr.Offset,SEEK_SET);
	switch(dib.Compression) {
		case 0://BI_RGB
			for (i=0;i<r->height;i++)
				fread(r->buffer+((r->height-(i+1))*r->width),1,r->width,f);
			break;
		//case 1://BI_RLE8
			//break;
		//case 1://BI_RLE4
			//break;
		default:
			fclose(f);
			printf("Error: Unsupported compression!\n");
			return NULL;
	}
	fclose(f);
	/*f = fopen("bmp.txt","wb");
	if (f!=NULL) {
		for (i=0;i<r->bufferlength;i++) {
			if ((i&0x0F)==0)
				fprintf(f,"\n%04x - ",i);
			fprintf(f,"%02X ",r->buffer[i]);
		}
		fclose(f);
	}*/
	return r;
}
