#include <malloc.h>

#include "_images.h"

#define GIF_MAGIC_NUMBER_A		0x38464947
#define GIF_MAGIC_NUMBER_B		0x6139
typedef struct {
	unsigned long MagicNumA;
	unsigned short MagicNumB;
	unsigned short ScreenWidth;
	unsigned short ScreenHeight;
	unsigned char GCTFlags;
	unsigned char BackgroundColor;
	unsigned char DefaultPixelAspect;
	unsigned char Palette[768];
	unsigned short GraphicControlExtension;
	unsigned char GCEDataWidth;
	unsigned char GCETransColor;
	unsigned short GCEAnimDelay;
	unsigned char GCTTransColor;
	unsigned char ImageDescriptor;
	unsigned short NorthWest[2];
	unsigned short Width;
	unsigned short Height;
	unsigned char LocalColorTableBit;
	unsigned char StartOfImageLZW;
	unsigned char AmountOfLZE;
} GIFHeader_t;

image_t *LoadGIF(const char *filename) {
	return NULL;
}
