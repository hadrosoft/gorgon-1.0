#ifndef _IMAGE_LIB_H
#define _IMAGE_LIB_H

#include <stdint.h>

typedef struct {
	uint32_t bufferlength;
	uint32_t width;
	uint32_t height;
	uint8_t *buffer;
	uint8_t *palette;
	uint16_t palettelength;
	uint8_t bpp;
} image_t;

image_t *LoadPCX(const char *filename);
image_t *LoadGIF(const char *filename);
image_t *LoadBMP(const char *filename);

#endif
